<?php
$mysqli = new mysqli("localhost", "root", "", "tools_test");

// Check connection
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli->connect_error;
    exit();
}

$PACKAGE_ID = 151;
$SHIPPER_HQ_ID = 283;

$map = [
    59=>350,
    58=>354,
    60=>355,
    57=>351,
    61=>357,
    62=>356,
    129=>358,
    278=>352,
    277=>353,
    315=>359
];

if ($result = $mysqli->query("SELECT * FROM catalog_product_entity_int WHERE attribute_id = $PACKAGE_ID")) {
    while ($row = $result->fetch_assoc()) {
        
        $value = $row['value'];
        $entityId = (int) $row['entity_id'];
        $storeId = (int) $row['store_id'];

        if(!isset($map[$value])) {
            throw new RuntimeException("Could not map val $value");
        }

        $newValue = $map[$value];

        $existing = $mysqli->query("SELECT * FROM catalog_product_entity_text WHERE attribute_id=$SHIPPER_HQ_ID 
            AND entity_id=$entityId AND store_id=$storeId");

        if($existing->num_rows) {
            $row = $existing->fetch_assoc();
            $valueId = $row['value_id'];
            
            // UPDATE
            $statement = $mysqli->prepare("UPDATE catalog_product_entity_text SET value=$newValue WHERE value_id=?");
            $statement->bind_param('d', $valueId);
            var_dump("U ");
        } else {
            $statement = $mysqli->prepare("INSERT INTO catalog_product_entity_text (`store_id`, `entity_id`, `attribute_id`, `value`) VALUES (?, ?, ?, ?)");
            $statement->bind_param('ddds', $storeId, $entityId, $SHIPPER_HQ_ID, $newValue);
            $statement->execute();
            var_dump("I ");
        }
        
    }
}
